import { RiComputerLine } from 'react-icons/ri';
import { FaServer } from 'react-icons/fa';
import { AiOutlineAntDesign, AiOutlineApi } from 'react-icons/ai';
import { BsCircleFill } from 'react-icons/bs';
import { MdDeveloperMode } from 'react-icons/md';
import { IProject, IService, ISkill } from './type';

export const services: IService[] = [
  {
    title: 'Frontend Development',
    about:
      'I can build a beautiful and scalable SPA with <b>HTML5</b>, <b>CSS3</b>, and <b>JavaScript</b>.',
    Icon: RiComputerLine,
  },
  {
    title: 'Backend Development',
    about:
      'Handle database, server, and API with <b>Node.js</b> and <b>Express</b>.',
    Icon: FaServer,
  },
  {
    title: 'API Development',
    about: 'I can develop RESTful API with <b>Laravel</b> and <b>Express</b>.',
    Icon: AiOutlineApi,
  },
  {
    title: 'Competitive Coder',
    about:
      'A daily practice of competitive programming with <b>Hackerrank</b> and <b>Codesignal</b>.',
    Icon: MdDeveloperMode,
  },
  {
    title: 'UX/UI Designer',
    about: 'Stuning and designing user interface with <b>Figma</b>.',
    Icon: AiOutlineAntDesign,
  },
  {
    title: 'Whatever',
    about: 'To do whatever do you want with <b>ReactJs</b> and <b>NextJs</b>.',
    Icon: RiComputerLine,
  },
];

export const langugues: ISkill[] = [
  {
    name: 'Javascript',
    level: '80',
    Icon: BsCircleFill,
  },
  {
    name: 'PHP',
    level: '70',
    Icon: BsCircleFill,
  },
  {
    name: 'Laravel',
    level: '70',
    Icon: BsCircleFill,
  },
  {
    name: 'ReactJs',
    level: '60',
    Icon: BsCircleFill,
  },
  {
    name: 'NodeJs',
    level: '50',
    Icon: BsCircleFill,
  },
  {
    name: 'NextJs',
    level: '50',
    Icon: BsCircleFill,
  },
];

export const tools: ISkill[] = [
  {
    name: 'Git',
    level: '80',
    Icon: BsCircleFill,
  },
  {
    name: 'Jira',
    level: '80',
    Icon: BsCircleFill,
  },
  {
    name: 'MacOs',
    level: '60',
    Icon: BsCircleFill,
  },
  {
    name: 'Figma',
    level: '50',
    Icon: BsCircleFill,
  },
  {
    name: 'Docker',
    level: '50',
    Icon: BsCircleFill,
  },
];

export const projects: IProject[] = [
  {
    id: 1,
    name: 'Warehouse Management System 1',
    description: 'A simple todo list with ReactJs.',
    image_path: '/images/wms.png',
    deployed_url: 'https://react-todo-list.netlify.app/',
    git_url: '',
    categories: ['php', 'laravel'],
    key_techs: ['PHP', 'Laravel', 'Angular'],
  },
  {
    id: 2,
    name: 'Transport Management System',
    description: 'A simple todo list with ReactJs.',
    image_path: '/images/tms.png',
    deployed_url: 'https://react-todo-list.netlify.app/',
    git_url: '',
    categories: ['php'],
    key_techs: ['ReactJs', 'NextJs', 'Styled-Components', 'Redux'],
  },
  {
    id: 3,
    name: 'Merchant Service Provider',
    description: 'A simple todo list with ReactJs.',
    image_path: '/images/msp.png',
    deployed_url: 'https://react-todo-list.netlify.app/',
    git_url: '',
    categories: ['node'],
    key_techs: ['ReactJs', 'NextJs', 'Styled-Components', 'Redux'],
  },
  {
    id: 4,
    name: 'Warehouse Management System 2',
    description: 'A simple todo list with ReactJs.',
    image_path: '/images/vms.png',
    deployed_url: 'https://react-todo-list.netlify.app/',
    git_url: '',
    categories: ['react', 'node'],
    key_techs: ['ReactJs', 'NextJs', 'Styled-Components', 'Redux'],
  },
  {
    id: 5,
    name: 'Company Website',
    description: 'A simple todo list with ReactJs.',
    image_path: '/images/company.png',
    deployed_url: 'https://react-todo-list.netlify.app/',
    git_url: '',
    categories: ['php'],
    key_techs: ['ReactJs', 'NextJs', 'Styled-Components', 'Redux'],
  },
  {
    id: 6,
    name: 'Travel Website',
    description: 'A simple todo list with ReactJs.',
    image_path: '/images/travel.png',
    deployed_url: 'https://airbnb-namdev.vercel.app',
    git_url: '',
    categories: ['php', 'laravel'],
    key_techs: ['ReactJs', 'NextJs', 'Styled-Components', 'Redux'],
  },
];
