import React, { FunctionComponent } from 'react';
import { Categories } from '../type';

export const NavItem: FunctionComponent<{
  value: Categories | 'all';
  handleFilterCategory: Function;
  active: string;
}> = ({ value, handleFilterCategory, active }) => {
  let classes = 'capitalize cursor-pointer hover:text-green';
  if (value === active) classes += ' text-green';

  return (
    <li className={classes} onClick={() => handleFilterCategory(value)}>
      {value}
    </li>
  );
};

const ProjectNavbar: FunctionComponent<{
  handleFilterCategory: Function;
  active: string;
}> = (props) => {
  return (
    <div className='flex px-3 py-2 space-x-2 overflow-x-auto list-none'>
      <NavItem {...props} value='all' />
      <NavItem {...props} value='react' />
      <NavItem {...props} value='php' />
      <NavItem {...props} value='laravel' />
      <NavItem {...props} value='node' />
    </div>
  );
};

export default ProjectNavbar;
