import { AiFillGithub, AiFillLinkedin, AiFillFacebook } from 'react-icons/ai';
import { GoLocation } from 'react-icons/go';
import { GiTie } from 'react-icons/gi';
import { useTheme } from 'next-themes';
import Image from 'next/image';

const Sidebar = () => {
  const { theme, setTheme } = useTheme();

  const changeTheme = () => {
    setTheme(theme === 'light' ? 'dark' : 'light');
  };

  return (
    <div>
      <Image
        src='/images/avatar.jpg'
        className='w-32 h-32 mx-auto rounded-full'
        width='128px'
        height='128px'
        layout='intrinsic'
        quality={100}
        alt='Avatar'
      />
      <h3 className='my-4 text-3xl font-medium tracking-wider font-kaushan'>
        <span className='text-green'>Nam</span> Dev
      </h3>
      <p className='px-2 py-1 my-3 bg-gray-200 rounded-full dark:bg-dark-200'>
        Web Developer
      </p>
      <a
        href=''
        className='flex items-center justify-center px-2 py-1 my-3 bg-gray-200 rounded-full dark:bg-dark-200'
      >
        <GiTie className='w-6 h-6' /> Download Resume
      </a>
      <div className="flex justify-around w-9/12 mx-auto my-5 text-green-500 md:w-full">
        <a
          href="https://github.com/namdevdk2710"
          target="_blank"
          rel="noreferrer"
        >
          <AiFillGithub className="w-8 h-8 cursor-pointer" />
        </a>
        <a
          href="https://www.linkedin.com/in/nam-nguyen-2a74b2180"
          target="_blank"
          rel="noreferrer"
        >
          <AiFillLinkedin className="w-8 h-8 cursor-pointer" />
        </a>
        <a
          href="https://www.facebook.com/namdev2710"
          target="_blank"
          rel="noreferrer"
        >
          <AiFillFacebook className="w-8 h-8 cursor-pointer" />
        </a>
      </div>
      <div
        className='py-4 my-5 bg-gray-200 dark:bg-dark-200'
        style={{ marginLeft: '-1rem', marginRight: '-1rem' }}
      >
        <div className='flex items-center justify-center space-x-2'>
          <GoLocation />
          <span>Da Nang city, Vietnam</span>
        </div>
        <p className='my-2'>namdevdk2710@gmail.com</p>
        <p className='my-2'>0334-366-133</p>
      </div>

      <button
        className='w-8/12 px-5 py-2 my-2 text-white rounded-full bg-gradient-to-r from-green to-blue-400 focus:outline-none'
        onClick={() => window.open('mailto:namdevdk2710@gmail.com')}
      >
        Email Me
      </button>
      <button
        onClick={changeTheme}
        className='w-8/12 px-5 py-2 my-2 text-white rounded-full bg-gradient-to-r from-green to-blue-400'
      >
        Light Mode
      </button>
    </div>
  );
};

export default Sidebar;
