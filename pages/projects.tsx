import Head from 'next/head';
import { useState } from 'react';
import { motion } from 'framer-motion';

import ProjectCard from '../components/ProjectCard';
import ProjectNavbar from '../components/ProjectNavbar';
import { projects as projectsData } from '../data';
import { Categories } from '../type';
import { fadeInUp, stagger, routeAnimation } from '../animations';

const Projects = () => {
  const [projects, setProjects] = useState(projectsData);
  const [active, setActive] = useState('all');
  const [showDetail, setShowDetail] = useState<number | null>(null);

  const handleFilterCategory = (category: Categories | 'all') => {
    if (category === 'all') {
      setProjects(projectsData);
      setActive(category);
      return;
    }

    let newProject = projectsData.filter((project) =>
      project.categories.includes(category)
    );
    setProjects(newProject);
    setActive(category);
  };

  return (
    <motion.div
      variants={routeAnimation}
      initial='initial'
      animate='animate'
      exit='exit'
    >
      <Head>
        <title>NamDev Portfolio | Projects</title>
      </Head>

      <div className='px-5 py-2 overflow-scroll' style={{ height: '65vh' }}>
        <ProjectNavbar
          handleFilterCategory={handleFilterCategory}
          active={active}
        />
        <motion.div
          className='relative grid grid-cols-12 gap-4 my-3'
          variants={stagger}
          initial='initial'
          animate='animate'
        >
          {projects.map((project, index) => (
            <motion.div
              variants={fadeInUp}
              key={index}
              className='col-span-12 p-2 bg-gray-200 rounded-lg sm:col-span-6 lg:col-span-4 dark:bg-dark-200'
            >
              <ProjectCard
                project={project}
                showDetail={showDetail}
                setShowDetail={setShowDetail}
              />
            </motion.div>
          ))}
        </motion.div>
      </div>
    </motion.div>
  );
};

export default Projects;
