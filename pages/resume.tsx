import Head from 'next/head';
import Bar from '../components/Bar';
import { langugues, tools } from '../data';
import { motion } from 'framer-motion';
import { fadeInUp, routeAnimation } from '../animations';

const resume = () => {
  return (
    <motion.div variants={routeAnimation} initial='initial' animate='animate'>
      <Head>
        <title>NamDev Portfolio | Resume</title>
      </Head>

      <div className='px-6 py-2'>
        <div className='grid gap-6 lg:grid-cols-2'>
          <motion.div variants={fadeInUp} initial='initial' animate='animate'>
            <h5 className='my-3 text-2xl font-bold'>Education</h5>
            <div>
              <h5 className='my-2 text-xl font-bold'>
                Computer Sciene Engineering
              </h5>
              <p className='font-semibold'>
                Academy of Information Technology, University of Technology,
              </p>
              <p className='my-3'>
                I am current studying Computer Science Engineering at the FPT
                Politechnic
              </p>
            </div>
          </motion.div>
          <motion.div variants={fadeInUp} initial='initial' animate='animate'>
            <h5 className='my-3 text-2xl font-bold'>Experience</h5>
            <div>
              <h5 className='my-2 text-xl font-bold'>Software Engineer Sr.</h5>
              <p className='font-semibold'>ST United (2019 - Present)</p>
              <p className='my-3'>
                Do whatever you want, but don&apos;t forget to be awesome.
              </p>
            </div>
          </motion.div>
        </div>

        <div className='grid gap-6 lg:grid-cols-2'>
          <div>
            <h5 className='my-3 text-2xl font-bold'>Languages & Frameworks</h5>
            <div className='py-2'>
              {langugues.map((language, index) => (
                <Bar data={language} key={index} />
              ))}
            </div>
          </div>
          <div>
            <h5 className='my-3 text-2xl font-bold'>Tools & Softwares</h5>
            <div className='py-2'>
              {tools.map((tool, index) => (
                <Bar data={tool} key={index} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

export default resume;
