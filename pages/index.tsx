import Head from 'next/head';
import ServiceCard from '../components/ServiceCard';
import { services } from '../data';
import { motion } from 'framer-motion';
import { fadeInUp, stagger, routeAnimation } from '../animations';

const Home = () => {
  return (
    <motion.div variants={routeAnimation} initial='initial' animate='animate'>
      <Head>
        <title>NamDev Portfolio</title>
      </Head>

      <div className="flex flex-col flex-grow px-6 pt-1">
        <h5 className="my-3 font-medium">
          I am a Software Engineer with 6+ years of experience facilitating
          cutting-edge engineering solutions with a wide range of Logistic
          application and technology skills. Proven ability to leverage
          full-stack knowledge and experience to build interactive and
          user-centered website designs to scale. Extensive expertise in large
          system architecture development and administration, as well as design
          and development.
        </h5>
        <div
          className='flex-grow p-4 mt-5 bg-gray-400 dark:bg-dark-100'
          style={{ marginLeft: '-1.5rem', marginRight: '-1.5rem' }}
        >
          <h5 className='my-3 text-xl font-bold tracking-wide'>
            What I am doing
          </h5>
          <motion.div
            className='grid gap-6 lg:grid-cols-2'
            variants={stagger}
            initial='initial'
            animate='animate'
          >
            {services.map((service, index) => (
              <motion.div
                variants={fadeInUp}
                key={index}
                className='bg-gray-200 rounded-lg dark:bg-dark-200 lg:col-span-1'
              >
                <ServiceCard service={service} />
              </motion.div>
            ))}
          </motion.div>
        </div>
      </div>
    </motion.div>
  );
};

export default Home;
